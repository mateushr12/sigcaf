// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

//inicia firebase
var firebaseConfig = {
    apiKey: "AIzaSyANRUK0QxBSbWQ-dhz1uMxAni1SbmOBAb4",
    authDomain: "sigcaf-a47dd.firebaseapp.com",
    databaseURL: "https://sigcaf-a47dd.firebaseio.com",
    projectId: "sigcaf-a47dd",
    storageBucket: "sigcaf-a47dd.appspot.com",
    messagingSenderId: "854426242480",
    appId: "1:854426242480:web:379972e8b58f1c8f"
}
firebase.initializeApp(firebaseConfig)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
